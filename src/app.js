const express = require('express')

module.exports = () => {
  const app = express()

  app.use((req, res) => {
    res.send('Hello World!')
  })

  return app;
}
