const supertest = require('supertest')

const app = require('./app')

describe('Server', () => {
  it('produces hello world message', async () => {
    const response = await supertest(app()).get('/').expect(200)
    expect(response.text).toBe('Hello World!')
  })
})
